const fetch = require('node-fetch'),
    cheerio = require('cheerio');

module.exports = function(url, callback) {

    try {
        var p = new URL(url);
    } catch (err) {
        return callback('Not a valid URL');
    }

    const protocol = (p.protocol) ? p.protocol : 'http:',
        tld = protocol + '//' + p.host,
        types = ['image/ico',
            'image/x-icon',
            'image/vnd.microsoft.icon',
            'image/png',
            'image/gif',
            'image/jpg',
            'image/svg'],
        usualSuspects = ['/favicon.ico'];

    // console.error('p: %o (%s)',p, protocol);

    let icon = url.replace(/\/$/, '');

    async function isFavicon(u) {
        try {
            const response = await fetch(u);
            return await response.ok && types.some(type => response.headers.get('content-type').includes(type));
        } catch (err) {
            return false;
        }
    }

    async function checkSuspects(f) {
        for (const suspect of usualSuspects) {
            if (await isFavicon(f + suspect)) return f + suspect;
        }
        return '';
    }

    async function checkTheDom(u) {
        const response = await fetch(u);
        const $ = cheerio.load(await response.text());
        if ($('link') && $('link').length > 0) {
            $('link').each(function() {
                if ($(this).attr('rel') && $(this).attr('rel') == 'icon') {
                    if (!$(this).attr('href').startsWith('http')) {
                        if ($(this).attr('href').startsWith('/')) {
                            if ($(this).attr('href').startsWith('//')) {
                                icon = protocol + $(this).attr('href');
                            } else {
                                icon = tld + $(this).attr('href');
                            }
                        }
                    } else {
                        icon = $(this).attr('href');
                    }
                }
            });
            return icon;
        } else {
            return '';
        }
    }

    async function checkAll() {

        if (await isFavicon(icon)) return icon;

        let res = await checkTheDom(icon);
        if (res) if (await isFavicon(res)) return res;

        // res = await checkSuspects(icon);
        // if (res) if (await isFavicon(res)) return res;

        res = await checkTheDom(tld);
        if (res) if (await isFavicon(res)) return res;

        res = await checkSuspects(tld);
        if (res) if (await isFavicon(res)) return res;

        return false;

    }

    (async function() {

        try {
            let res = await checkAll();
            return (res) ? callback(null, res) : callback('No icon found')
        } catch (err) {
            return callback(err);
        }

    })();

};
