# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.0.12](https://framagit.org/yphil/favrat/compare/v0.0.13...v0.0.12)

### Commits

- CI tests, since Internet is really lame back here I don't even really know what's going on during the tests [`99c46ba`](https://framagit.org/yphil/favrat/commit/99c46ba35d9ddf051ee7e73f99be44b10ce83d8a)
- Testing auto-changelog [`4827fed`](https://framagit.org/yphil/favrat/commit/4827fed16da03dba916e985c182e7dd36376ed64)
- Hackish url testing OK [`897f500`](https://framagit.org/yphil/favrat/commit/897f5002886984532b8f4c81b3c490d94b3388d5)

## [v0.0.13](https://framagit.org/yphil/favrat/compare/0.0.12...v0.0.13) - 2021-02-20

### Commits

- Testing auto-changelog [`2be205b`](https://framagit.org/yphil/favrat/commit/2be205b1397f63251eec09e470c12c358ee82f03)
- Version 0.0.11 "First major release using the async / await paradigm" - 20/02/2021 14:14:16 [`4c011d0`](https://framagit.org/yphil/favrat/commit/4c011d038f430025f086b2e1df2faf0023c8ef05)

## [0.0.12](https://framagit.org/yphil/favrat/compare/v0.0.12...0.0.12) - 2021-02-20

### Commits

- CI tests, since Internet is really lame back here I don't even really know what's going on during the tests [`99c46ba`](https://framagit.org/yphil/favrat/commit/99c46ba35d9ddf051ee7e73f99be44b10ce83d8a)
- Testing auto-changelog [`4827fed`](https://framagit.org/yphil/favrat/commit/4827fed16da03dba916e985c182e7dd36376ed64)
- Hackish url testing OK [`897f500`](https://framagit.org/yphil/favrat/commit/897f5002886984532b8f4c81b3c490d94b3388d5)

## v0.0.12 - 2021-02-20

### Commits

- Initial scrap OK, usual suspects OK [`f22559a`](https://framagit.org/yphil/favrat/commit/f22559a5aa26c143b0cd8623968c6f0490eeed1e)
- Libs version bump [`ab63ac3`](https://framagit.org/yphil/favrat/commit/ab63ac3537344978a3bc2bad80849ab3c1f83de9)
- Sys: Bootstrapping [`3135101`](https://framagit.org/yphil/favrat/commit/313510112614ca9f78e141da09fad29665656f5d)
