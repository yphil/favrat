const should = require('should'),
    favrat = require(__dirname + '/../');

describe('favrat', function() {

    it('returns a favicon link as-is', function(done) {
        favrat('http://www.lemonde.fr/favicon.ico', function(err, url) {
            if (err) return done(err);
            url.should.eql('http://www.lemonde.fr/favicon.ico');
            done();
        });
    });

    it('returns an SVG favicon link', function(done) {
        favrat('https://css-tricks.com/favicon.svg', function(err, url) {
            if (err) return done(err);
            url.should.eql('https://css-tricks.com/favicon.svg');
            done();
        });
    });

    it('parses a long and somewhat messy link rel=icon node list', function(done) {
        favrat('https://css-tricks.com/', function(err, url) {
            if (err) return done(err);
            url.should.containEql('https://i0.wp.com/css-tricks.com/wp-content/uploads/2021/07/star.png');
            done();
        });
    });

    it('finds a favicon from the root', function(done) {
        favrat('http://www.lemonde.fr/', function(err, url) {
            if (err) return done(err);
            url.should.eql('http://www.lemonde.fr/dist/assets/img/logos/pwa-180.png');
            done();
        });
    });

    it('finds a favicon by crawling back from the URL to the root', function(done) {
        favrat('http://www.lefigaro.fr/rss/figaro_flash-actu.xml', function(err, url) {
            if (err) return done(err);
            url.should.eql('http://www.lefigaro.fr/favicon-16x16.png');
            done();
        });
    });

    it('finds a relative favicon link in the DOM and returns a valid URL', function(done) {
        favrat('https://coolors.co/', function(err, url) {
            if (err) return done(err);
            url.should.eql('https://coolors.co/assets/img/favicon.png');
            done();
        });
    });

    it('ignore false positives - 404 errors on usualSuspects', function(done) {
        favrat('https://exode.me/feeds/videos.xml?videoChannelId=484', function(err, url) {
            if (err) return done(err);
            url.should.containEql('https://exode.me/client/assets/images/favicon.png');
            done();
        });
    });

    it('handles 404 errors', function(done) {
        favrat('http://rss.liberation.fr/rss/latest/', function(err, url) {
            if (err) return done();
            err.should.eql('Error: No icon found');
            done();
        });
    });

    it('does not break when the url is in error', function(done) {
        favrat('https://news.gooogle.com/news/rss/rss', function(err, url) {
            if (err) return done();
            return done();
        });
    });

    it('finds a protocol-relative favicon URL and returns a good one', function(done) {
        favrat('https://news.google.com/topstories?hl=en-US&gl=US&ceid=US:en', function(err, url) {
            if (err) return done(err);
            url.should.eql('https://ssl.gstatic.com/gnews/logo/google_news_40.png');
            done();
        });
    });

});
