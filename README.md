# Favrat

*Search, find and report a web page's (fav)icon*

---

[![pipeline status](https://framagit.org/yphil/favrat/badges/master/pipeline.svg)](https://framagit.org/yphil/favrat/-/pipelines)
[![coverage](https://framagit.org/yphil/favrat/badges/master/coverage.svg)](https://framagit.org/yphil/favrat/-/pipelines)
[![website](https://img.shields.io/website?down_message=down&up_color=brightgreen&up_message=up&url=https%3A%2F%2Fpetrolette.space)](https://petrolette.space)
[![Liberapay](https://img.shields.io/liberapay/receives/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Liberapay](https://img.shields.io/liberapay/goal/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Ko-Fi](https://img.shields.io/badge/Ko--fi-F16061?label=buy+me+a&style=flat&logo=ko-fi&logoColor=white)](https://ko-fi.com/yphil/tiers)

## Installation

`npm install favrat`

## Testing

`npm test`

## Usage

```javascript

var favrat = require('favrat');

favrat("http://nodejs.org/", function(err, favicon_url) {

    if (favicon_url) {
        res.send(feed)
    } else {
        res.status(500).send('No feed found')
    }

});

```
Brought to you ♫ by [yPhil](http://yphil.bitbucket.io/) Consider ♥ [helping](https://liberapay.com/yPhil/donate) icon by Mattahan (Paul Davey)
